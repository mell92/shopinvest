<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\CartController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/fiche-produit/{id}', [ProduitController::class, 'getProduit'])->name('fiche-produit');;
Route::post('/creation/delete-image', [ProduitController::class, 'deleteImage']);
Route::get('/creation/{id?}', [ProduitController::class, 'creation'])->name('fiche-backOffice');
Route::post('/creation/{id?}', [ProduitController::class, 'postCreation'])->name('user.postCreation');
Route::resource('/panier', CartController::class)->only(['index', 'store', 'update', 'destroy']);
Route::post('/suppression', [ProduitController::class, 'suppression']);
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');