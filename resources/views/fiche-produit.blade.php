@extends('layouts.app')
@section('content')



@if(isset($produit))
@include('layouts.file-ariane', ['nomProduit' => $produit->nom])

<a href="{{ route('fiche-backOffice', ['id' => $produit->id]) }}" class="pl-3">
    <button type="button" class="btn btn-warning mb-2">Modifier la fiche produit</button>
</a>

<div id="fiche-produit" class="col-12 d-flex border">
    <div class="col-5 pl-0" id="carousel">
    @isset($produit->images)
    @if (count($produit->images) > 0)
        <div class="master">
            <img src="{{$produit->images->first()->image_filename}}">
        </div>
        <div class="thumbnails">
            @foreach($produit->images as $image)
                <img src="{{$image->image_filename}}" alt="3 suisses">
            @endforeach   
        </div> 
    @else 
        <div class="master">
            <img src="/images/image-icone.png">
        </div>

    @endif	
    @endisset	

    </div>
    <div class="col-7" id="infos-produit">
        <div class="row en-tete">
            <div class="col-9">
                <h1>{{$produit->nom}}</h1>
                <h2 class="mt-5">Marque : {{$produit->marque->nom}}</h2>
            </div>
            <div class="col-3 prix-fiche-produit d-flex align-items-center" style="height: 50px;">
                <span id="prix-barre" class="mr-2">{{$produit->prix}} €</span>
                <p class="m-0">
                {{ str_replace(".", ",", $produit->prix - ($produit->prix * 5)/100)}} €    
                </p>
            </div>     
        </div>

        <div class="row infos-complementaires">
            <div class="col-12 d-flex mt-4 mb-2 justify-content-start">
                <button id="clickDescription" class="btn bg-button-fiche mr-2">Description</button>
                <button id="clickLivraison" class="btn bg-button-fiche mr-2">Livraison</button>
                <button id="clickGaranties" class="btn bg-button-fiche mr-2">Garanties et Paiement</button>
            </div>
            <div class="col-12">
                <div id="info-selected" class="p-2">
                    <div id="description">
                        {{$produit->description}}
                    </div>
                    <div id="livraison" style="display:none;">
                        Tout produit en stock est expédié sous 24h, hors week-ends et jours fériés. Si le produit est en réapprovisionnement, le délai de livraison est indiqué sur la fiche produit.

                        Une fois le colis remis au transporteur choisi par le client, les délais de livraison sont les suivants pour la France
                    </div>
                    <div id="garanties" style="display:none;">
                        Indépendamment de la garantie commerciale, et conformément à la réglementation, LIVELLE s'engage à respecter ses obligations relatives aux garanties attachées aux produits vendus sur son site et dans ses différents catalogues, qui figurent dans les articles extraits du Code de la Consommation et du Code Civil 
                    </div>
                </div>
            </div>

            <form class="form-inline mt-4 w-100 justify-content-between col-12 d-flex" action="{{ route('panier.store')}}" method="POST">
                @csrf
                
                <input type="hidden" id="idProduit" name="id" value="{{ $produit->id }}">
                <input type="hidden" name="prix" value="{{($produit->prix - ($produit->prix * 5)/100) }}">
                <div class="col-2 p-2">
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button type="button" class="quantity-left-minus btn btn-info btn-number"  data-type="minus" data-field="">
                            <span class="glyphicon glyphicon-minus">-</span>
                        </button>
                        </span>
                        <input type="text" id="quantity" name="quantite" class="form-control input-number" value="1" min="1" max="100">
                        <span class="input-group-btn">
                        <button type="button" class="quantity-right-plus btn btn-info btn-number" data-type="plus" data-field="">
                            <span class="glyphicon glyphicon-plus">+</span>
                        </button>
                        </span>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Ajouter au panier</button>
            </form>    
        </div>
    </div>       
</div>
@else  
    @include('layouts.file-ariane')  
    <div class="btn btn-danger mt-5">  
        <p>Ce produit n'existe plus!</p>
        <a href="{{ route('home') }}" class="text-white">Retourner à l'accueil du site </a>
    </div>
@endif

@stop

@push('scripts')
<script>

$(function() {
    
    // Bouton quantité
    var quantitiy=0;
   $('.quantity-right-plus').click(function(e){
        
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
            
            $('#quantity').val(quantity + 1);

          
            // Increment
        
    });

     $('.quantity-left-minus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
      
            // Increment
            if(quantity>0){
            $('#quantity').val(quantity - 1);
            }
    });

    //make the width of the thumbnails images is dynamic
    var imagesNumber        = $(".thumbnails").children().length,
        marginBetweenImages =  1,
        totalMargins        = marginBetweenImages * (imagesNumber - 1),
        imageWidth          = (100 - totalMargins) / (imagesNumber);
        
    $(".thumbnails img").css({
        marginRight: marginBetweenImages + "%"
    });

    $(".thumbnails img").on("click", function() {
        $(this).addClass("active").siblings().removeClass("active");
        $(".master img").hide().attr("src", $(this).attr("src")).fadeIn(300);
    });

})

// Afficher les infos cliquées
$('#clickDescription').on('click',function(){
    $('#livraison').hide();
    $('#garanties').hide();
    $('#description').show();
});

$('#clickLivraison').on('click',function(){
    $('#description').hide();
    $('#garanties').hide();
    $('#livraison').show();
});

$('#clickGaranties').on('click',function(){
    $('#description').hide();
    $('#livraison').hide();
    $('#garanties').show();
});


</script>

@endpush
