<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test Shopinvest</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    </head>
    <body class="antialiased pl-4 pr-4">
        @if (Route::has('login'))
            <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                @auth
                    <a href="{{ url('/home') }}" class="text-sm text-gray-700 underline">Home</a>
                @else
                    <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Log in</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                    @endif
                @endauth
            </div>
        @endif

        <header id="header" class="row mr-0 ml-0">
            <a href="" id="logo" class="col-sm-3"></a>
            <a href="" id="icon-account" class="col-sm-3 offset-sm-3"></a>
            <a href="" id="icon-cart" class="col-sm-3"></a>
        </header>

        <div class="row m-0 menu">
            <div class="col-12 d-flex">
                <p>> Accueil ></p>
                <p>Femme ></p>
                <p>Top ></p>
                <p>a remplir dynamiquement</p>
            </div>
        </div>

        <div id="fiche-produit" class="col-12 d-flex border" style="height: 600px">
            <div class="col-5" id="carousel">
                <img src="{{URL::asset('/images/livy.jpg')}}" class="w-100">

            </div>
            <div class="col-7" id="infos-produit">
                <div class="row en-tete">
                    <div class="col-6">
                        <h1>TITRE</h1>
                        <h2>Marque</2>
                    </div>
                    <div class="col-6">
                        <p>PRIX</p>
                    </div>     
                </div>

                <div class="row infos-complementaires">
                    <div class="col-12 d-flex">
                        <button>Description</button>
                        <button>Livraison</button>
                        <button>Garanties et Paiement</button>
                    </div>
                    <div id="info-selected">

                    </div>

                    <form class="form-inline">
                        <div class="form-group">
                        <label for="exampleFormControlSelect1">Quantité</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Ajouter</button>
                    </form>    
                </div>

            </div>

        </div>
        


    </body>
    @stack('scripts')
    
</html>
