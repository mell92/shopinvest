@extends('layouts.app')
@section('content')
<div id="creation" class="p-4">

	@isset($error)
		<div class="alert alert-primary" role="alert">
			{{$error}}
		</div>
	@endisset
	
	{!! Form::model($produit ?? '', array('route' => array('user.postCreation'), 'files' =>true, 'method' =>'post' )) !!}
	{!! csrf_field()!!}
	

		<div class="col-12 d-flex align-items-around">
			<h1>Informations générales</h1>
			@isset($produit->id)
				<div class="btn btn-danger ml-auto p-2" onclick="deleteProduit({{ $produit->id }})">Supprimer le produit</div>
			@endisset
		</div>
		

      	<div class="row bg-light">
			<div class="col-12">

			@isset($produit->id)
				<input type="hidden" name="id" value = "{{$produit->id}}">
			@endisset

				<div class="form-group">
					<label>Nom</label>
					{!! Form::text('nom', null, 
						[
						'class' => 'form-control',
						'required'
						])
					!!}
				</div>

				<div class="col-4 pl-0">
					<h5>Marque</h5>
					<div id="marque-id"> 
					@if(count($marques) > 0 )
					<label>Sélectionner la marque</label>
						<select class="form-control" name="marque_id">	
							@foreach($marques as $marque)
								@isset($produit->marque->id)
									@if ($marque->id == $produit->marque->id)							
										<option value="{{$marque->id}}" selected>{{$marque->nom}}</option>
									@else
										<option value="{{$marque->id}}">{{$marque->nom}}</option>
									@endif
								@else
									<option value="{{$marque->id}}">{{$marque->nom}}</option>	
								@endisset			
							@endforeach
						</select>	
					@else
						<label>Saisir une marque</label>
						<input required  type='text' class='form-control' id='input_marque' name='marque'>
						@endif	
					</div>
					
					@if(count($marques) > 0 )
						<p class="titre-marque bg-info-marque mt-2">Si la marque n'est pas présente, cliquez <span id="ajout-marque" class="text-primary pointer font-weight-bold">ICI</span> pour la renseigner</p>
					@endif
					<div id="conteneur-input-marque"></div>
				</div>


                <div class="row mt-4 bg-light">
                    <div class="form-group col-sm-12">
                        <label for="exampleTextarea">Description</label>
                        {!! Form::textarea('description', null, 
                            [
                            'rows' => 8,
							'class' => 'form-control',
							'required'
                            ])
                        !!}
                    </div>
                </div>
                
                <div class="col-6 pl-0">
                    <label>Prix</label>
                    {!! Form::text('prix', null, 
                        [
						'class' => 'form-control',
						'required'
                        ])
                    !!}
				</div>

				<h3 class="mt-5">Ajouter des nouvelles images au produit</h3>
				<input
					type="file"
					multiple
					name="files[]"
					label="Déposez vos fichiers ou cliquez ici."
					is="drop-files"
				/>		
				
				<div class="pl-3 border border-info mt-4" >
					@isset($produit->images)
						@if (count($produit->images) > 0)
						<h3 class="mt-5">Images déjà enregistrées en Base donnée <br>-> supprimer en cliquant sur la croix</h3>
						<div class="col-12 d-flex">
							@foreach($produit->images as $image)	
								<div class="col-sm-2" id="imageId{{ $image->id }}">
									<img class="img-form"src="{{$image->image_filename}}" alt="3 suisses"/>
									<span class="bg-danger w-100 d-block text-center pointer" onclick="deleteImage({{ $image->id }})">X</span>							
								</div>
							@endforeach
						</div>		
						@endif	
					@endisset			
				</div>

    </div>

			</div>	

  			<button type="submit" class="btn btn-primary mt-5 send">Enregistrer</button>
      	</div>
		
	{!! Form::close() !!}

</div>
@stop

@push('scripts')
<script>

window.Laravel = 
{!! json_encode(['csrfToken' => csrf_token(),]) !!};
window.token = Laravel.csrfToken;

function deleteImage(id)
{
	$.post('/creation/delete-image', {
		idImage :id,
		_token: token
	})

	.done(function(){
		//Supprimer le conteneur HTML avec l'image
		$( "#idImage"+id ).remove();
		location.reload();
	})
}

$('#ajout-marque').on('click',function(){
	$("#conteneur-input-marque").prepend("<input required  type='text' class='form-control' id='input_marque' name='marque'>");
	$('#marque-id').empty();
	$('.titre-marque').empty();
	$('#input_marque').show();
	
});

function deleteProduit(id)
{
	$.post('/suppression', {
		idProduit :id,
		_token: token
	})

	.done(function(){
		alert("le produit est supprimé");
		location.href = "/"; 
	})

}


</script>

@endpush
