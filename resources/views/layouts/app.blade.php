<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Test Shopinvest</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- include jQuery library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
</head>
<body>
    <header id="header" class="row mr-0 ml-0">
        <div class="col-sm-3 align-self-center">
            <a href="" id="logo" >
                <img src="/images/logo3s.svg" alt="logo 3 suisses"/>
            </a>
        </div>
            
        <!-- Authentication Links -->
        @guest
            @if (Route::has('login'))
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Se connecter</a>
                </li>
            @endif

            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">Créer un compte</a>
                </li>
            </ul>    
            @endif
        @else
            <div class="col-sm-3 offset-sm-4 mt-3">
                <a href="" id="icon-account" class=""></a>
                <ul class="text-right">
                    <li class="nav-item dropdown list-unstyled">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle p-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('fiche-backOffice') }}">Création produit</a>    
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    Se déconnecter
                                </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>                           
        @endguest
        <div class="col-2 mt-3 text-center">
            <a href="" id="icon-cart"></a>
            @if ($cartCount)
                <span>{{ $cartCount }} article </span> @if($cartCount > 1)s @endif <br> <span> Montant : {{ number_format($cartTotal, 2, ',') }}€ </span>
            @endif
            
        </div>          
    </header>
    @yield('content')
    <script type="module" src="//unpkg.com/@grafikart/drop-files-element"></script>
</body>
@stack('scripts')
</html>
