@extends('layouts.app')
@section('content')

@include('layouts.file-ariane')

<div>
    <div class="row">

    @if( count($produits) > 0)
    <div class="col-12 cards-container d-flex">
      @foreach($produits as $produit)
        <div class="card col-3 mr-3 p-2">
          <div class="card-image">
              <a href="{{ route('fiche-produit', ['id' => $produit->id]) }}" class="w-100">
                  @if(isset($produit->images->first()->image_filename))
                    <img class="image-home" src="{{$produit->images->first()->image_filename}}" alt="3 suisses">
                  @else
                    <img class="image-home mt-4 mb-3" src="/images/image-icone.png" alt="3 suisses">
                  @endif     
             </a>
          </div>          
          <div class="card-content center-align">
            <p class="font-weight-bold">{{ $produit->nom }}</p>         
          </div>
          <div class="d-flex">
                    <p><strong>{{ number_format($produit->prix, 2, ',', ' ') }} € TTC</strong></p>
                </div>
        </div>
    @endforeach
    </div>
    @else
    <div class="alert alert-warning" role="alert">
        Aucun produit enregistré en BDD !
    </div>
    @endif
    
  </div>
</div>
@stop
