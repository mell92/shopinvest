<?php

namespace App\Services;

use App\Models\Produit;
use App\Models\Image;
use App\Service;
use App\Repository\ProduitRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ProduitService
{
    protected $produitInterface;

    public function getProduit($id)
    {
        $produit = Produit::with('images')->with('marque')->find($id);
        return $produit;
    }

    public function saveProduitInBdd($id, $request, $marque_id)
    {
        $produit = ($request->id) ? Produit::find($request->id) : new Produit;
        if(!is_null($marque_id))
        {
            $produit->marque_id = $marque_id;
        }
        $produit->fill($request->except('_token'));
        $produit->save();

        if ($request->hasFile('files')) {

            foreach($request->file('files') as $k => $v) {
                $path = Storage::disk('public')->putFile('images', $request->file('files')[$k]);
                $filename = basename($path);
                $result['url'] = $filename;

                $folder = "/storage/images";
                $photo = new Image();
                $photo->produit_id = $produit->id;
                $photo->image_filename = $folder. '/' . $filename;
                $photo->save();
            }           
        }  

        return $produit;

    }

    public function getAllProduits()
    {
        $produits = Produit::all();

        return $produits;
    }

    public function deleteImage($idImage)
    {
        $image = Image::find($idImage);
        $image->delete();
    }

    public function deleteProduit($idProduit)
    {
        $produit = Produit::with('images')->find($idProduit);
        $produit->delete();

    }


}