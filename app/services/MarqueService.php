<?php

namespace App\Services;

use App\Models\Produit;
use App\Models\Marque;
use App\Service;
use App\Repository\ProduitRepositoryInterface;
use Illuminate\Http\Request;

class MarqueService
{

    public function getAllMarques()
    {
        $marques = Marque::all();

        return $marques;
    }

    public function saveMarqueInBdd($marque)
    {
        $nom = strtoupper($marque);
        $marque = new Marque;
        $marque->nom = $nom;
        $marque->save();

        return $marque;

    }


}