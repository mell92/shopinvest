<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProduitService;
use Cart;

class CartController extends Controller
{
    public function __construct( ProduitService $produitService)
    {
        $this->produitService = $produitService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $content = Cart::getContent();
        $total = Cart::getTotal();

        return view('panier.index', compact('content', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produit = $this->produitService->getProduit($request->id);
        
        Cart::add([
            'id' => $produit->id,
            'name' => $produit->nom,
            'price' => $request->input('prix'),
            'quantity' => $request->input('quantite'),
            'attributes' => [],
            'associatedModel' => $produit,
        ]
        );
        return redirect()->back()->with('cart', 'ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
