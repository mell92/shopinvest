<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Services\ProduitService;
use App\Services\MarqueService;
use App\Models\Image;
use Illuminate\Support\Facades\Storage;



class ProduitController extends Controller
{

    public function __construct( ProduitService $produitService, MarqueService $marqueService)
    {
        $this->produitService = $produitService;
        $this->marqueService = $marqueService;
    }

    public function getProduit($id)
    {
        $produit = $this->produitService->getProduit($id);
    
        return view('fiche-produit')->with(
            ['produit' => $produit]
        );  
    }

    public function creation(Request $request, $id=null)
    {
        $produit = $this->produitService->getProduit($id);
        $marques = $this->marqueService->getAllMarques();

        return view('creation')->with(
            ['marques' => $marques,
            'produit' => $produit
            ]
        );

    }

    public function postCreation(Request $request, $id=null)
    {  

        if($request->marque && isset($request->marque))
        {
            $marque = $this->marqueService->saveMarqueInBdd($request->marque);
            $marque_id = $marque->id;
            $produit = $this->produitService->saveProduitInBdd($id, $request, $marque_id);
        }
        else {
            $produit = $this->produitService->saveProduitInBdd($id, $request, $marque_id = null);
        }

        return redirect()->to('fiche-produit/'.$produit->id);   
    }

    public function deleteImage(Request $request)
    {  
        $produit = $this->produitService->deleteImage($request->idImage);
    }
    
    public function suppression(Request $request) 
    {
        $produit = $this->produitService->deleteProduit($request->input('idProduit'));
    
    }
}
