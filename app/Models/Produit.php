<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    protected $fillable = ['nom', 'prix', 'marque_id', 'description'];

    public function marque()
    { 
        return $this->belongsTo(Marque::class); 
    }

    public function images() 
    { 
        return $this->hasMany(Image::class); 
    }
}
