# SHOPINVEST TEST

Exercice Test SHOPINVEST

### Pré-requis

- PHP 
- BDD MYSQL
- COMPOSER
- NPM

### Installation

- Création d'un Vhost
- Création d'une base de données
- git clone du projet sur Gitlab

Installation des dépendances du projet
- composer install
- npm install
- configurer le fichier .env en mettant les informations de connexion à la BDD

## Démarrage

Lancer le server :
- cd my-project/
 php artisan serve

- Créer les tables en BDD
 php artisan migrate

Pour Build les CSS et JS :
- npm run watch

## Tester le projet

- Pour info, la homepage correspond à l'onglet "tops" du fil d'ariane. Sur cette page sont affichés tous les produits présents en BDD.

-  Commencer par créer un compte utilisateur (si pas connecté, la homepage redirige vers login), dans le menu en haut de page vous avez le lien pour vous enregister. 
Après la connexion ou création de compte, vous êtes redirigé vers la homepage.

- Dans le menu du Header, sous l'icone, cliquer sur votre nom et sélectionner "création produit", cela vous redirige vers la page de création de produit.
Une fois la création finalisée, vous êtes redirigé vers la fiche produit

- Depuis la fiche produit : 
    - vous pouvez ajouter l'article à votre panier qui sera automatiquement mis à jour en fonction du prix et de la quantité
    - Modifier la fiche produit en cliquant sur le bouton "Modifier la fiche produit"
    - Supprimer le produit en cliquant sur le bouton "Modifier la fiche produit" la fiche produit puis ensuite sur "Supprimer le produit"

##  Remarques

Les images sont stockées dans le dossier storage/app/public et un lien symbolique à été créé de public/storage vers storage/app/public gràce à la commande avec la commande "php artisan storage:link"

## Fabriqué avec

Laravel 8
